using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public Text score;
    public Text life;
    public Text time;
    public GameObject buttonWin;
    public GameObject buttonDefeat;
    // Start is called before the first frame update
    void Start()
    {
        score.text = "Score: 0";
        life.text = "Life: 3";
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void GlobalScore(int score)
    {
        this.score.text = "Socore: " + score.ToString();
    }
    public void UILifes(int life)
    {
        this.life.text = "Life: " + life.ToString();
    }
    public void UiTimer(float timer)
    {
        time.text = "Time: " + ((int)timer).ToString();
    }

    public void DefeatButton()
    {
        buttonDefeat.SetActive(true);
    }
    public void WinButton()
    {
        buttonWin.SetActive(true);
    }

}
