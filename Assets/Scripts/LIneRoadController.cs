using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LIneRoadController : MonoBehaviour
{
    public bool check;
    [SerializeField]
    private GameManager gamemanager;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!check && other.tag == "Player")
        {
            check = true;
            gamemanager.AddScore(10);
        }
    }

}
