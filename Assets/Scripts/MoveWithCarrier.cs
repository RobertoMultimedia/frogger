using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWithCarrier : MonoBehaviour
{
    CharacterController player;
    Vector3 groundPosition;
    Vector3 lastGroundPosition;
    string groundName;
    string lastGroundName;
    // Start is called before the first frame update
    void Start()
    {
        player = this.GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        FollowGround();
    }

    private void FollowGround()
    {
        if (player.isGrounded)
        {

            RaycastHit hit;
            if (Physics.SphereCast(transform.position, player.height / 4.2f, -transform.up, out hit))
            {
                
                GameObject groundedIn = hit.collider.gameObject;
                groundName = groundedIn.name;
                groundPosition = groundedIn.transform.position;


                if (groundPosition != lastGroundPosition && groundName == lastGroundName)
                {
                    player.enabled = false;
                    this.transform.position += (groundPosition - lastGroundPosition)/2;
                    player.enabled = true;

                }

                lastGroundName = groundName;
                lastGroundPosition = groundPosition;

            }


        }
        else if (!player.isGrounded)
        {
            lastGroundName = null;
            lastGroundPosition = Vector3.zero;
        }
    }

    private void OnDrawGizmos()
    {
        player = this.GetComponent<CharacterController>();
        Gizmos.DrawWireSphere(transform.position, player.height);
    }
}
