using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private int life;
    [SerializeField]
    private int score;
    [SerializeField]
    private int frogCount;
    [SerializeField]
    private LIneRoadController[] lineScore;
    [SerializeField]
    private UIManager uiManager;
    [SerializeField]
    private PlayerController player;

    private float timer = 40f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SetTime();
    }

    public void Default()
    {
        life = 3;
        score = 0;
        ResetTimer();

    }

    public void DecreaseLife()
    {
        if (life > 0)
        {
            life--;
            ResetTimer();
            uiManager.UILifes(life);
        }
        else if(life <= 0)
        {
            Defeat();
        }
    }

    public void ResetTimer()
    {
        timer = 40f;
    }


    public void Defeat() 
    {
        uiManager.DefeatButton();
        Time.timeScale = 0;
    }

    public void FrogCount()
    {
        frogCount++;
        ResetLineScore();
        ResetTimer();
        if (frogCount == 5)
        {
            Time.timeScale = 0;
            uiManager.WinButton();
        }
    }
    public void AddScore(int score)
    {
        this.score += score;
        uiManager.GlobalScore(this.score);

    }

    public void ResetLineScore()
    {
        for (int i = 0; i < lineScore.Length; i++)
        {
            lineScore[i].check = false;
        }
    }
    public void SetTime()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
            uiManager.UiTimer(timer);

        }
        else
        {
            print("Time out");
            ResetTimer();
            DecreaseLife();
            player.PositionReset();
        }
    }

    public void LoadScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Game");
    }
}
