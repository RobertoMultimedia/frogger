using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float gravity = -9.8f;

    public int speed;
    private Vector3 playerInput;
    private CharacterController player;
    private Vector3 movePlayer;

    [SerializeField]
    private Camera mainCamera;
    private Vector3 camRight;
    private Vector3 camForward;

    [SerializeField]
    private GameManager gameManager;

    private bool check;


    private void Start()
    {
        player = GetComponent<CharacterController>();
    }
    private void Update()
    {
        Movement();
    }
    public void Movement()
    {

        float verticalInput = Input.GetAxisRaw("Vertical");
        float horizontalInput = Input.GetAxisRaw("Horizontal");

        playerInput = new Vector3(horizontalInput, gravity, verticalInput);
        playerInput = Vector3.ClampMagnitude(playerInput, 1);

        SetDirectionCamera();

        movePlayer = playerInput.x * camRight +playerInput.z * camForward;

        player.transform.LookAt(player.transform.position + movePlayer);
        SetGravity();

        player.Move(movePlayer * speed * Time.deltaTime);

    }

    void SetDirectionCamera()
    {
        camForward = mainCamera.transform.forward;
        camRight = mainCamera.transform.right;

        camForward.y = 0;
        camRight.y = 0;

        camForward = camForward.normalized;
        camRight = camRight.normalized;

    }

    void SetGravity() 
    {
        movePlayer.y = gravity * Time.deltaTime;
    }

  
    public void HitEnemy()
    {
        PositionReset();
        gameManager.DecreaseLife();
        gameManager.ResetTimer();
    }
    public void PositionReset()
    {
        player.enabled = false;
        this.transform.position = new Vector3(0, 1.231f, 0);
        player.enabled = true;
    }

}
