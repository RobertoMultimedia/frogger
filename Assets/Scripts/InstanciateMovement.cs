using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciateMovement : MonoBehaviour
{
    [SerializeField]
    private int speed = 5;
    private GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LiveTime());
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }
    
    IEnumerator LiveTime()
    {
        yield return new WaitForSeconds(20);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<PlayerController>().HitEnemy();
        }
    }
}
