using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastLine : MonoBehaviour
{
    [SerializeField]
    private GameObject frog;
    [SerializeField]
    private GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            frog.SetActive(true);
            other.GetComponent<PlayerController>().PositionReset();
            gameManager.FrogCount();
        }
    }
}
