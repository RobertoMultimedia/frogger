using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject prefab;
    [SerializeField]
    private int minSpawnTime;
    [SerializeField]
    private int maxSpawnTime;
 
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnPrefab());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator SpawnPrefab()
    {
        int time = Random.Range(minSpawnTime, maxSpawnTime);

        yield return new WaitForSeconds(time);

        Instantiate(prefab, transform.position, transform.rotation);
        
        StartCoroutine(SpawnPrefab());
    }
}
