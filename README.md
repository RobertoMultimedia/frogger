Unity version 2020.3.26f1

Assets

Buildings:
Lowpoly Modern City Buildings Set
https://assetstore.unity.com/packages/3d/environments/urban/lowpoly-modern-city-buildings-set-64427

Trees:
Cartoon low poly tree
https://assetstore.unity.com/packages/3d/props/exterior/cartoon-low-poly-tree-211305

Carrier:
Navy planes and carrier pack
https://assetstore.unity.com/packages/3d/vehicles/air/navy-planes-and-carrier-pack-170259

Tank:
The War Gods: pack of military voxels vol.1
https://assetstore.unity.com/packages/3d/vehicles/land/the-war-gods-pack-of-military-voxels-vol-1-126505

Frog:
Stone Frog
https://assetstore.unity.com/packages/3d/characters/stone-frog-18022



